const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const Router = require('@koa/router')
const serve = require('koa-static')

const app = new Koa()
const router = new Router()

router.post('/deep-analytics', (ctx) => {
  // mirror our post data
  ctx.body = JSON.parse(ctx.request.body.json)
})

app.use(serve('public'))
app.use(bodyParser())
app.use(router.routes()).use(router.allowedMethods())

const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log(`Serving http://localhost:${port}/`)
})