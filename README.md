# Passing state cross-domain
This is an experiment with ways to pass client state from a page in 1 domain (or subdomain) to another.

There's a description of 2 approaches that were tested here:
https://whitespacehealth.atlassian.net/wiki/spaces/~914995624/pages/1611825180/Deep+Analytics+-+Apply+Filters+from+Practice+Analytics

- Using `window.postMessage` browser API
- Using form post with hidden field containing JSON data

Both of the experiments here make use of a little `koa` web app `app.js`.

## window.postMessage API
Frontend code for 2 different domains can be found in
- `public\from.html`
- `public\to.html`

You will need to run 2 instances of the `koa` server with different ports to mimick different subdomains.

In 1 terminal run:
`set PORT=3000 && yarn start`

In another run:
`set PORT=5000 && yarn start`

- Open http://localhost:3000/from.html
- You should see another tab open to http://localhost:5000/to.html with a JSON string representing filters.

## Form Post with Hidden JSON Field
- Run the api `yarn start`
- Open http://localhost:3000/practice.html
- Click the button
- Another tab should open showing JSON string that was posted